function cargarDatos(url) {
    const http = new XMLHttpRequest();

    http.onreadystatechange = function () {
        // ... (el resto del código sigue igual)
    };

    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnBuscarPorId").addEventListener('click', function () {
    const inputId = document.getElementById('inputId').value;
    const url = `https://jsonplaceholder.typicode.com/albums/${inputId}`;
    cargarDatos(url);
});
