// app.js
function cargarDatos(url) {
    const http = new XMLHttpRequest();

    http.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            let res = document.getElementById('lista');
            const json = JSON.parse(this.responseText);

            // Limpiar resultados anteriores
            res.innerHTML = "";

            // Mostrar resultados
            if (Array.isArray(json)) {
                // Caso de búsqueda de todos los datos
                for (const datos of json) {
                    res.innerHTML += '<tr><td class="columna1">' + datos.userId + '</td>' +
                        '<td class="columna2">' + datos.id + '</td>' +
                        '<td class="columna3">' + datos.title + '</td></tr>';
                }
            } else {
                // Caso de búsqueda por ID
                res.innerHTML += '<tr><td class="columna1">' + json.userId + '</td>' +
                    '<td class="columna2">' + json.id + '</td>' +
                    '<td class="columna3">' + json.title + '</td></tr>';
            }
        } else if (this.status == 404 && this.readyState == 4) {
            alert("ID no encontrado");
        } else {
            alert("Surgió un error al hacer la petición");
        }
    };

    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnBuscarTodos").addEventListener('click', function () {
    const url = "https://jsonplaceholder.typicode.com/albums";
    cargarDatos(url);
});

document.getElementById("btnBuscarPorId").addEventListener('click', function () {
    const inputId = document.getElementById('inputId').value;
    const url = `https://jsonplaceholder.typicode.com/albums/${inputId}`;
    cargarDatos(url);
});

document.getElementById("btnLimpiar").addEventListener('click', function () {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});
